package com.webdemo.test.users;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;

import com.webdemo.dal.model.Users;
import com.webdemo.service.muliti.impl.MulitiServiceImpl;
import com.webdemo.service.user.impl.UserServiceImpl;
import com.webdemo.test.BaseServiceTest;


//@Transactional
public class UserServiceTest extends BaseServiceTest{
	
	@Resource(name = "userServiceImpl")
	UserServiceImpl userServiceImpl;
	
	@Resource(name = "mulitiServiceImpl")
	MulitiServiceImpl mulitiServiceImpl;
	
	//测试两条插入语句事务回滚
	@Test
	public void testMulitiSqlTranc() {
		mulitiServiceImpl.insertOneDbTranc();
	}
	
	/**
	 * 测试获取自增主键
	  * <h2>方法注释说明</h2>
	  * @Title: test1
	  * @Description: TODO
	  * @param 
	  * @return void    返回类型
	  * @throws
	 */
	@Test
	public void test1() {
		try{
			Users user = new Users();
			user.setUserId(0);
			user.setUsername("test1_s");
			user.setPassword("test1_s");
			
			//temp 受影响的行数
			int temp = userServiceImpl.insert(user);
			if(temp==1){
				System.out.println("插入成功!");
				System.out.println("刚插入的自增主键是："+user.getUserId());
			}else{
				System.out.println("插入失败!");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	//测试mybatis foreach语句运行
	@Test
	public void test2() {
		try{
			List<String> userList = new ArrayList<String>();
			userList.add("test1_u");
			userList.add("test1_s");
			List<Users> l = userServiceImpl.testForeach(userList);
			System.err.println(l.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//调用存储过程测试
	@Test
	public void testP() {
		try{
			Map<String, Integer> param = new HashMap<String, Integer>();  
            param.put("username",1); 
            param.put("result",-1); 
			userServiceImpl.testP(param);
			System.err.println(param.get("result"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//测试mybatis mapper.xml里面的if 为空或字符串条件
	@Test
	public void testParam() {
		try{
			Users user = new Users();
			user.setUserId(0);
			user.setUsername("");
			user.setPassword("test1_s");
			userServiceImpl.selectByParam(user);
			System.err.println(user.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
