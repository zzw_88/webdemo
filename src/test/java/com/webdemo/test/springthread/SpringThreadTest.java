package com.webdemo.test.springthread;

import java.util.concurrent.RejectedExecutionException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;

import com.webdemo.test.BaseServiceTest;

public class SpringThreadTest extends BaseServiceTest{
    //线程池
    @Autowired
    //@Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Test
    public void test() {
    	for(int i = 0; i < 25; i++) {     
	       try{
	            taskExecutor.execute(new TaskThread(""+(i+1)));
	        } catch (RejectedExecutionException e) {
	           //do something
	        }
    	}
    }
}
class TaskThread implements Runnable {
	private String name;

	public TaskThread(String name){
		this.name = name;
	}
    
    @Override
    public void run() {
	  System.err.println(name+"do some thing！");
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
