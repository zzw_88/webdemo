package com.webdemo.test.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.springframework.web.util.WebUtils;

import com.webdemo.test.BaseServiceTest;

public class WebUtil extends BaseServiceTest{
	HttpServletRequest request;
	//@Test
	public void testWebUtils(){
		//过滤请求方法request.getParameterMap()返回值中的含munu_参数的值
		Map<String,Object> map = WebUtils.getParametersStartingWith(request,"munu_");
		
		//获取 HttpServletRequest 中特定名字的 Cookie 对象
		String name = "cookieName";
		Cookie ck = WebUtils.getCookie(request, name);
		
		//获取 HttpSession 特定属性名的对象
		name = "sessionName";
		Object o = WebUtils.getSessionAttribute(request,name);
		
		//强制要求 HttpSession 中拥有指定的属性，否则抛出异常
		o = WebUtils.getRequiredSessionAttribute(request, name); 
		
		//获取 Session ID 的值
		String sessionId = WebUtils.getSessionId(request);
		
		//将 Map 元素添加到 ServletRequest 的属性列表中，当请求被导向（forward）到下一个处理程序时，这些请求属性就可以被访问到了；
		Map attributes = null;
		WebUtils.exposeRequestAttributes(request,attributes);
		
		//ServletContext相关方法
		
		//获取相对路径对应文件系统的真实文件路径
		String path = "";
		ServletContext servletContext = null;
		try {
			String  paths = WebUtils.getRealPath(servletContext, path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//获取 ServletContex 对应的临时文件地址
		File tempDir = WebUtils.getTempDir(servletContext);
		
		
		
	}

}
