package com.webdemo.test.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.webdemo.dal.dao.one.UserMapper;
import com.webdemo.dal.model.Users;


/**
  * <h1>管理员账户信息服务实现</h1>
  * @ClassName: UserServiceImpl
  * @Description: 管理员账户信息服务实现
  * @author Zdm-xwq
  * @date 2015年8月24日 上午9:34:07
  *
 */
@Service
public class TestCacheImpl {
	
	@Autowired
	private UserMapper userMapper;
	
	/**
	  * <h2>管理员账户信息的删除方法</h2>
	  * @Description: 管理员账户信息的删除方法
	  * @param 	id
	  * @return int    返回类型
	 */
	@CacheEvict(value = "user", allEntries = true)  
	public int deleteByPrimaryKey(Integer userId) {
		int result = userMapper.deleteByPrimaryKey(userId);
		return result;
	}

	/**
	  * <h2>管理员账户信息的查询方法</h2>
	  * @Description: 管理员账户信息的查询方法
	  * @param id
	  * @return int    返回类型
	 */
	@Cacheable(value = "user")
	public Users selectByPrimaryKey(Integer userId) {
		Users user = userMapper.selectByPrimaryKey(userId);
		return user;
	}

	@Cacheable(value = "user",condition = "#user==null?false:true")
	public List<Users> selectByParam(Users user) {
		return userMapper.selectByParam(user);
		
	}

	@CacheEvict(value = "user", allEntries = true)  
	public int insert(Users user1) {
		return userMapper.insert(user1);
	}

	
}
