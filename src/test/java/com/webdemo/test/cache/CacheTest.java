package com.webdemo.test.cache;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.webdemo.dal.model.Users;
import com.webdemo.test.BaseServiceTest;


public class CacheTest extends BaseServiceTest {
	
	@Resource
	TestCacheImpl  testCacheImpl;
	
	@Test
	public void testSelectCache(){
		Users user = new Users();
		user.setUserId(1);
		Long start1 = System.currentTimeMillis();
		testCacheImpl.selectByParam(user);
		Long end1 = System.currentTimeMillis();
		System.err.println("第一次耗时："+(end1-start1)+"毫秒");
		
		Long start = System.currentTimeMillis();
		testCacheImpl.selectByParam(user);
		Long end = System.currentTimeMillis();
		System.err.print("第二次耗时："+(end-start)+"毫秒");
		if((end-start)>(end1-start1)){
			System.err.println("没有用上缓存!");
		}else{
			System.err.println("用上了缓存!");
		}
	}
	
	@Test
	public void testInsertUpdateDeleteCache(){
		Users user = new Users();
		Long start1 = System.currentTimeMillis();
		testCacheImpl.selectByParam(user);
		Long end1 = System.currentTimeMillis();
		System.err.println("第一次耗时："+(end1-start1)+"毫秒");
		
		Long start = System.currentTimeMillis();
		testCacheImpl.selectByParam(user);
		Long end = System.currentTimeMillis();
		System.err.print("第二次耗时："+(end-start)+"毫秒");
		if((end-start)>(end1-start1)){
			System.err.println("没有用上缓存!");
		}else{
			System.err.println("用上了缓存!");
		}
		
		Users user1 = new Users();
		user.setUserId(0);
		user1.setUsername("test1_u");
		user1.setPassword("test1_p");
		testCacheImpl.insert(user1);
		System.err.println("插入了一条记录");
		
		Long start2 = System.currentTimeMillis();
		List<Users> temp = testCacheImpl.selectByParam(user);
		Long end2= System.currentTimeMillis();
		System.err.print("第三次耗时："+(end2-start2)+"毫秒");
		if((end2-start2)>(end1-start1)){
			System.err.println("没有用上缓存!");
			
		}else{
			System.err.println("用上了缓存!");
		}
		for(Users user2:temp){
			System.err.println(user2.getUsername());
		}
	}
	
	@Test
	public void testNullCache(){
		System.err.println("参数为空测试。。。。。。");
		Long start = System.currentTimeMillis();
		testCacheImpl.selectByParam(null);
		Long end= System.currentTimeMillis();
		System.err.print("第一次耗时："+(end-start)+"毫秒");
		if(end-start>0){
			System.err.println("没有用上缓存!");
		}else{
			System.err.println("用上了缓存!");
		}
		System.err.println("再次调用。。。。。。");
		Long start2 = System.currentTimeMillis();
		testCacheImpl.selectByParam(null);
		Long end2= System.currentTimeMillis();
		System.err.print("第二次耗时："+(end2-start2)+"毫秒");
		if(end2-start2>5){
			System.err.println("没有用上缓存!");
		}else{
			System.err.println("用上了缓存!");
		}
		
	}
}
