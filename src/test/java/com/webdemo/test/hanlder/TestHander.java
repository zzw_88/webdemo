package com.webdemo.test.hanlder;

import javax.annotation.Resource;

import org.junit.Test;

import com.webdemo.dal.dao.one.BooleansMapper;
import com.webdemo.dal.model.Booleans;
import com.webdemo.test.BaseServiceTest;

public class TestHander extends BaseServiceTest{
	@Resource
	private BooleansMapper booleansMapper;
	
	//测试类型转换器
	@Test
	public void insert(){
		Booleans b = new Booleans();
		b.setId(0);
		b.setBclomun(false);
		booleansMapper.insertTest(b);
	}
}
