package com.webdemo.test.onetomany;


import javax.annotation.Resource;

import org.junit.Test;

import com.webdemo.dal.model.Member;
import com.webdemo.dal.model.Orders;
import com.webdemo.service.member.impl.MemberServiceImpl;
import com.webdemo.service.order.impl.OrderServiceImpl;
import com.webdemo.test.BaseServiceTest;

/**
 * @author Administrator
 * 
 */
public class MemberAndOrderTest extends BaseServiceTest{
	
	@Resource
	private MemberServiceImpl memberService;

	@Resource
	private OrderServiceImpl orderService;
	
	//一对多，查询person（一）级联查询订单order（多）
	@Test
	public void testSelectPersonFetchOrder() throws Exception {
		Member person = memberService.selectPersonFetchOrder(1);
		System.out.println(person);
		System.out.println(person.getOrderList().size());
		for(Orders orders : person.getOrderList()){
			System.out.println(orders);
		}
	}
	
	//多对一，查询订单order（多）级联查询person（一）
	@Test
	public void testSelectOrdersFetchPerson() throws Exception{
		Orders orders = orderService.selectOrdersFetchPerson(1);
		System.out.println(orders);
		System.out.println(orders.getPerson());
	}
	
}
