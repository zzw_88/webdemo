package com.webdemo.test.mulitidb.change;

import javax.annotation.Resource;

import org.junit.Test;

import com.webdemo.service.muliti.impl.MulitiServiceImpl;
import com.webdemo.test.BaseServiceTest;

public class MulitiDbChangeTest extends BaseServiceTest {
	@Resource
	private MulitiServiceImpl mulitiServiceImpl;
	
	//测试两个库切换
	@Test
	public void insertMulitiChangeTranc(){
		mulitiServiceImpl.insertMulitiChangeTranc();
	}
	
	//测试跨两个库事务
	@Test
	public void insertMulitiTranc(){
		mulitiServiceImpl.insertMulitiTranc();
	}
}
