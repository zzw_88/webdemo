/*
 * Copyright: Copyright (c) 2014-2015 ZhongDaMen,Inc.All Rights Reserved. 
 * Company:中大门网络科技有限公司
 */

package com.webdemo.util;

import java.util.Map;


/**
 * <h1>数据处理</h1>
 * @ClassName: DataUtils
 * @Description: 数据处理
 * @author zdm-maoyongbin
 * @date Aug 19, 2015 10:12:24 AM
 *
 */
public class DataUtils {
	
	/**
	  * <h2>处理异常反回结果</h2>
	  * @Title: handleResultMap
	  * @Description: 处理异常反回结果
	  * @param @param resultMap
	  * @param @param exceptionEnum
	  * @return void    返回类型
	 */
	public static void handleResultMap(Map<String,Object> resultMap, SystemCodeEnum sysCodeEnum){
		resultMap.put("resultSuccess", sysCodeEnum.getSuccess());
		resultMap.put("resultCode", sysCodeEnum.getCode());
		resultMap.put("resultDesc", sysCodeEnum.getDesc());
	}
}
