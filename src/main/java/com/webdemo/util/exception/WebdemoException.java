package com.webdemo.util.exception;

public class WebdemoException extends Exception {

	private static final long serialVersionUID = -3548463252969713755L;

 	public WebdemoException() {
        super();
    }

    public WebdemoException(String message) {
        super(message);
    }

    public WebdemoException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebdemoException(Throwable cause) {
        super(cause);
    }
}
