package com.webdemo.util.mulitichange;
public abstract class DbChangeContextHolder {
 
    public final static String DATA_SOURCE_ORACLE = "dataSrcKey";
    public final static String DATA_SOURCE_MYSQL = "dataSrc1Key";
    
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();  
    
    public static void setDbChange(String customerType) {  
        contextHolder.set(customerType);  
    }  
      
    public static String getDbChange() {  
        return contextHolder.get();  
    }  
      
    public static void clearDbChange() {  
        contextHolder.remove();  
    }  
}