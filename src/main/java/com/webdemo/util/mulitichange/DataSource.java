package com.webdemo.util.mulitichange;
 
import java.lang.annotation.*;
 
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default DataSource.master;
 
    public static String master = "dataSrcKey";
 
    public static String slave1 = "dataSrc1Key";
 
}