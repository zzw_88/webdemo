package com.webdemo.util.limit;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;


/** 
 * TODO Comment of AbstractSpringMonitor 
 * @author 百恼 2013-4-9下午04:42:04 
 * 
 */  
public abstract class AbstractSpringMonitor implements MethodInterceptor,MonitorHandler{  
  
    /* (non-Javadoc) 
     * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation) 
     */  
    @Override  
    public Object invoke(MethodInvocation method) throws Throwable {  
        boolean result = before();  
        if(result){  
            try{  
                method.proceed();  
            }catch(Exception e){  
                  
            } finally{  
                after();  
            }  
        }  
        return null;  
    }  
}  