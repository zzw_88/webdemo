package com.webdemo.util;

public enum SystemCodeEnum {
	
		/** 数据操作成功 */
		DATA_SUCCESS("DEMO100","数据操作成功",true),
		/** 数据保存出错 */
		DATA_INSERT("DEMO101","数据保存出错！",false),
		/** 数据删除出错 */
		DATA_DELETE("DEMO102","数据删除出错！",false),
		/** 数据修改出错 */
		DATA_UPDATE("DEMO103","数据修改出错！",false),
		/** 数据查询出错 */
		DATA_SELECT("DEMO104","数据查询出错！",false),
		/** 参数传递出错 */
		DATA_PARAM("DEMO105","参数传递出错！",false);
		
		/** 异常代码 */
		public String code;
		/** 异常描述*/
		public String desc;
		/** 异常结果 */
		public Boolean success;
		
		SystemCodeEnum(String code, String desc, Boolean success){
			this.code = code;
			this.desc = desc;
			this.success = success;
		}

		/**
		 * 获取异常代码
		 * @return String
		 */
		
		public String getCode() {
			return code;
		}

		/**
		 * 设置异常代码
		 * @param code
		 */
		
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * 获取异常描述
		 * @return String
		 */
		
		public String getDesc() {
			return desc;
		}

		/**
		 * 设置异常描述
		 * @param desc
		 */
		
		public void setDesc(String desc) {
			this.desc = desc;
		}

		/**
		 * 获取异常结果
		 * @return Boolean
		 */
		
		public Boolean getSuccess() {
			return success;
		}

		/**
		 * 设置异常结果
		 * @param success
		 */
		
		public void setSuccess(Boolean success) {
			this.success = success;
		}
}
