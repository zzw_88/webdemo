package com.webdemo.dal.model;

public class Orders {
	 private int id;  
	    private double price;  
	    private Member person;  
	      
	    public Member getPerson() {  
	        return person;  
	    }  
	  
	    public void setPerson(Member person) {  
	        this.person = person;  
	    }  
	  
	    public int getId() {  
	        return id;  
	    }  
	  
	    public void setId(int id) {  
	        this.id = id;  
	    }  
	  
	    public double getPrice() {  
	        return price;  
	    }  
	  
	    public void setPrice(double price) {  
	        this.price = price;  
	    }  
	  
	    @Override  
	    public String toString() {  
	        return "Orders [id=" + id + ", price=" + price + "]";  
	    }  
	  
	    public Orders() {  
	        super();  
	    }  
}
