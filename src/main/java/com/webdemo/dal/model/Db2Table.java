package com.webdemo.dal.model;

public class Db2Table {
	private Integer id;
	private String desc1;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDesc1() {
		return desc1;
	}
	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}
	
	@Override
	public String toString() {
		return "Db2Table [id=" + id + ", desc=" + desc1 + "]";
	}
}
