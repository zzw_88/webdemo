package com.webdemo.dal.model;

public class Booleans {
	private int id;
	private Boolean bclomun;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Boolean getBclomun() {
		return bclomun;
	}
	public void setBclomun(Boolean bclomun) {
		this.bclomun = bclomun;
	}
	@Override
	public String toString() {
		return "Booleans [id=" + id + ", bclomun=" + bclomun + "]";
	}
}
