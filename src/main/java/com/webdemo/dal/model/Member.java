package com.webdemo.dal.model;

import java.util.List;

public class Member {
	 private int id;  
	    private String name;  
	    private List<Orders> orderList;  
	  
	    public int getId() {  
	        return id;  
	    }  
	  
	    public void setId(int id) {  
	        this.id = id;  
	    }  
	  
	    public String getName() {  
	        return name;  
	    }  
	  
	    public void setName(String name) {  
	        this.name = name;  
	    }  
	  
	    public List<Orders> getOrderList() {  
	        return orderList;  
	    }  
	  
	    public void setOrderList(List<Orders> orderList) {  
	        this.orderList = orderList;  
	    }  
	  
	    @Override  
	    public String toString() {  
	        return "Member [id=" + id + ", name=" + name + "]";  
	    }  
	  
	    public Member() {  
	        super();  
	    }  
	  
	    public Member(int id, String name, List<Orders> orderList) {  
	        super();  
	        this.id = id;  
	        this.name = name;  
	        this.orderList = orderList;  
	    }  
}
