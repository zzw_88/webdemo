package com.webdemo.dal.dao.one;

import com.webdemo.dal.model.Member;

public interface MemberMapper {
	public Member selectPersonFetchOrder(Integer id);
}
