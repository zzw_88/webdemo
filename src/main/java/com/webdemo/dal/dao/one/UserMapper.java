package com.webdemo.dal.dao.one;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.webdemo.dal.model.Users;

public interface UserMapper {
   public int deleteByPrimaryKey(Integer userId);
   public Users selectByPrimaryKey(Integer userId);
   public int insert(Users record);
   public List<Users> selectByParam(Users user);
   //mybatis多参数示例
   public Users testMulitiParam(@Param("username") String username,@Param("password") String password);
   //测试foreach 批量操作
   public List<Users> testForeach(List<String> userList);
   //测试存储过程
   public Integer testP(Map<String, Integer> p);
   
   //测试sql注入
   public List<Users> testIntejParam(String  userId);
}
