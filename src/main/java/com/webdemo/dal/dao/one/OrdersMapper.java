package com.webdemo.dal.dao.one;


import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;

import com.webdemo.dal.model.Orders;

public interface OrdersMapper {
	public Orders selectOrdersFetchPerson(int id);
	public Orders testMulitiParam(@Param("pId") int pid,@Param("price") BigDecimal price);
}
