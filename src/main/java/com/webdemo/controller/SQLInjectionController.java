package com.webdemo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SQLInjectionController {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@RequestMapping(value="/testI", method = {RequestMethod.GET})
	public String sqlInjection1(HttpServletRequest request){
		return "sqli";
	}
	
	@RequestMapping(value="/testInjection", method = {RequestMethod.POST})
	public ModelAndView sqlInjection(HttpServletRequest request){
		String userId = request.getParameter("userId");
		String sql = "select * from users where userId="+userId;
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
		ModelAndView mdl = new ModelAndView();
		mdl.addObject("users", list);
		mdl.setViewName("sqlit");
		return mdl;
	}
}
