package com.webdemo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webdemo.dal.model.Users;
import com.webdemo.util.DateEditor;

@Controller
public class IndexController {
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String indexS(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("session id名称:"+request.getSession().getId());
		System.out.println("session id名称:"+request.getRequestedSessionId());
		System.out.println("session是否来自cookie:"+request.isRequestedSessionIdFromCookie());
		System.out.println("session是否来自url重写:"+request.isRequestedSessionIdFromURL());
		System.out.println("session是不是新的:"+request.getSession().isNew());
		System.out.println("session创建时间:"+request.getSession().getCreationTime());
		System.out.println("session最后一次访问时间:"+request.getSession().getLastAccessedTime());
		return "index";
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response) {
		return "index";
	}
	
	@RequestMapping(value = "/muma", method = RequestMethod.GET)
	public String muma(HttpServletRequest request, HttpServletResponse response) {
		return "muma";
	}
	
	//Map请求后直接输出json
	@RequestMapping(value = "/mapToJson", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,String> mapToJson(HttpServletRequest request, HttpServletResponse response) {
		Map<String,String> m = new HashMap<String,String>();
		m.put("a", "a_value");
		m.put("b", "b_value");
		m.put("c", "c_value");
		return m;
	}
	
	//list对象请求后直接输出json
	@RequestMapping(value = "/listToJson", method = RequestMethod.GET)
	@ResponseBody
	public List<Users> listToJson(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Users> user = new ArrayList<Users>();
		Users u1 = new Users();
		Users u2 = new Users();
		u1.setPassword("a");
		u1.setUserId(1);
		u1.setUsername("a");
		
		u2.setPassword("b");
		u2.setUserId(1);
		u2.setUsername("b");
		user.add(u1);
		user.add(u2);
		return user;
	}
	
	//自定义对象请求后直接输出json
	@RequestMapping(value = "/objectToJson", method = RequestMethod.GET)
	@ResponseBody
	public Users objectToJson(HttpServletRequest request, HttpServletResponse response) {
		Users u = new Users();
		u.setUserId(1);
		u.setPassword("1");
		u.setUsername("2");
		return u;
	}
	
	@RequestMapping(value = "/jsonToObject", method = RequestMethod.GET)
	public String jsonObject(Users user,HttpServletRequest request, HttpServletResponse response) {
		return "jsonToObject";
	}
	
	//json请求后转自定义对象(异步请求)
	@RequestMapping(value = "/jsonToObject", method = RequestMethod.POST)
	@ResponseBody
	public Users jsonToObject(Users user,HttpServletRequest request, HttpServletResponse response) {
		System.out.println(user.getUserId());
		System.out.println(user.getUsername());
		System.out.println(user.getPassword());
		System.out.println(user.getNowDate());
		return user;
	}
	
	//json请求后转自定义对象(页面提交)
	@RequestMapping(value = "/jsonToObjectForPage", method = RequestMethod.POST)
	@ResponseBody
	public Users jsonToObjectForPage(Users user,HttpServletRequest request, HttpServletResponse response) {
		return user;
	}
	
	//json请求后转list对象(异步请求)
	@RequestMapping(value = "/jsonToList", method = RequestMethod.GET)
	public String jsonToList(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("session id名称:"+request.getSession().getId());
		System.out.println("session id名称:"+request.getRequestedSessionId());
		System.out.println("session是否来自cookie:"+request.isRequestedSessionIdFromCookie());
		System.out.println("session是否来自url重写:"+request.isRequestedSessionIdFromURL());
		System.out.println("session是不是新的:"+request.getSession().isNew());
		System.out.println("session创建时间:"+request.getSession().getCreationTime());
		System.out.println("session最后一次访问时间:"+request.getSession().getLastAccessedTime());
		return "jsonToList";
	}
		
	//json字符串请求后转list对象(异步请求)，加@ResponseBody，ajax中注意contentType参数
	@RequestMapping(value = "/jsonToList",produces="application/json; charset=utf-8",method = RequestMethod.POST)
	@ResponseBody
	public String jsonToList1(@RequestBody List<Users> user,HttpServletRequest request, HttpServletResponse response) {
		for(Users u:user){
			System.out.println(u.toString());
		}
		return "{\"success\":\"执行成功!\"}";
	}
	
	//返回Model
	@RequestMapping(value = "/model", method = RequestMethod.GET)
	public Model model(HttpServletRequest request, HttpServletResponse response) {
		Model m = new ExtendedModelMap();
		m.addAttribute("a", "a_value");
		return m;
	}
	
	//返回ModelAndView
	@RequestMapping(value = "/modelview", method = RequestMethod.GET)
	public ModelAndView modelAndView(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView m = new ModelAndView();
		m.addObject("a", "a_value");
		m.setViewName("ModeAndView");
		return m;
	}
	
	//测试session
	@RequestMapping(value = "/testSession", method = RequestMethod.GET)
	public String testSession(HttpServletRequest request, HttpServletResponse response) {
		return "session";
	}
		
	//测试session
	@RequestMapping(value = "/testSession", method = RequestMethod.POST)
	@ResponseBody
	public String testSessionPost(HttpServletRequest request, HttpServletResponse response) {
		return "success";
	}
	
	
	/**
	 * 
	  * <h2>注册日期编辑器</h2>
	  * @Title: initBinder
	  * @Description: 注册日期编辑器
	  * @param request
	  * @param binder
	  * @throws Exception
	 */
	@InitBinder
	protected void initBinder(HttpServletRequest request,ServletRequestDataBinder binder) throws Exception {
	    //对于需要转换为Date类型的属性，使用DateEditor进行处理
	    binder.registerCustomEditor(Date.class, new DateEditor());
	}
	
}
