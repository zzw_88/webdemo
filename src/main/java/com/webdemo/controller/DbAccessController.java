package com.webdemo.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.webdemo.dal.model.Users;
import com.webdemo.service.muliti.impl.MulitiServiceImpl;
import com.webdemo.service.user.impl.UserServiceImpl;

//测试数据库访问
@Controller
public class DbAccessController {
	
	@Resource
	private UserServiceImpl userServiceImpl;
	
	@Resource
	private MulitiServiceImpl mulitiServiceImpl;
	
	@RequestMapping(value = "testDbAccess", method = RequestMethod.GET)
	private void a(HttpServletRequest request, HttpServletResponse response) throws IOException{
		Users u = userServiceImpl.selectByPrimaryKey(1);
		response.getWriter().println(u.toString());
	}
	
	@RequestMapping(value = "testMuliti", method = RequestMethod.GET)
	private void b(HttpServletRequest request, HttpServletResponse response) throws IOException{
		mulitiServiceImpl.insertMulitiTranc();
	}
}
