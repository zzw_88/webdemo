package com.webdemo.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.webdemo.view.PdfRevenueReportView;
//import com.webdemo.view.ViewExcel;

@Controller
public class DownloadController {
	@RequestMapping(value = "filedownload", method = RequestMethod.GET)
	public String filedownload(HttpServletRequest request, HttpServletResponse response) {
		return "file";
	}
	
	/*@RequestMapping(value = "filedownload1", method = RequestMethod.GET)
	public View filedownload1(ModelMap model,HttpServletRequest request, HttpServletResponse response) {
       ViewExcel viewExcel = new ViewExcel();    
       return viewExcel;   
	}*/
	@RequestMapping(value = "filedownloadpdf", method = RequestMethod.GET)
	public View filedownload2(ModelMap model,HttpServletRequest request, HttpServletResponse response) {
		Map<String,String> revenueData = new HashMap<String,String>();
		revenueData.put("1/20/2010", "我是中文");
		revenueData.put("1/21/2010", "$200,000");
		revenueData.put("1/22/2010", "$300,000");
		revenueData.put("1/23/2010", "$400,000");
		revenueData.put("1/24/2010", "$500,000");
		model.addAttribute("revenueData", revenueData);
	    PdfRevenueReportView viewPdf = new PdfRevenueReportView();    
       return viewPdf;   
	}
}
