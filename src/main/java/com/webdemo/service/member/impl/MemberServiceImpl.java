package com.webdemo.service.member.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webdemo.dal.dao.one.MemberMapper;
import com.webdemo.dal.model.Member;
import com.webdemo.service.member.IMemberService;
@Service
public class MemberServiceImpl implements IMemberService{
	@Autowired
	private MemberMapper memberMapper;
	
	public Member selectPersonFetchOrder(int id){
		return memberMapper.selectPersonFetchOrder(id);
	}
}
