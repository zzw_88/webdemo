package com.webdemo.service.member;

import com.webdemo.dal.model.Member;

public interface IMemberService {
	public Member selectPersonFetchOrder(int id);
}
