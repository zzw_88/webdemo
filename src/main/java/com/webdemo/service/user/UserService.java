package com.webdemo.service.user;


import java.util.List;
import java.util.Map;

import com.webdemo.dal.model.Users;

public interface UserService {
	 public int deleteByPrimaryKey(Integer userId);
	 public Users selectByPrimaryKey(Integer userId);
	 public Users testMulitiParam(String username,String password);
	 int insert(Users user);
	 public List<Users> testForeach(List<String> userList);
	 public Integer testP(Map<String, Integer> p);
	 public List<Users> selectByParam(Users user);
	 public List<Users> testIntejParam(String userId);
}

