package com.webdemo.service.user.impl;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webdemo.dal.dao.one.UserMapper;
import com.webdemo.dal.model.Users;
import com.webdemo.service.user.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;
	
/*	@Autowired
	private JdbcTemplate jdbcTemplate;*/

	@Override
	public int deleteByPrimaryKey(Integer userId) {
		return userMapper.deleteByPrimaryKey(userId);
	}
	@Override
	public Users selectByPrimaryKey(Integer userId) {
		return userMapper.selectByPrimaryKey(userId);
	}
	
	@Override
	public int insert(Users user) {
		return userMapper.insert(user);
	}
	
	@Override
	public Users testMulitiParam(String username,String password){
		return userMapper.testMulitiParam(username, password);
	}
	@Override
	public List<Users> testForeach(List<String> userList) {
		return userMapper.testForeach(userList);
	}
	@Override
	public Integer testP(Map<String, Integer> p) {
		return userMapper.testP(p);
	}
	
	@Override
	public List<Users> selectByParam(Users user){
		return userMapper.selectByParam(user);
	}
	
	@Override
	public List<Users> testIntejParam(String userId){
		return userMapper.testIntejParam(userId);
	}
	
}
