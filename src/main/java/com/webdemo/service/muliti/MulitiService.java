package com.webdemo.service.muliti;

import com.webdemo.dal.model.Db2Table;

public interface MulitiService {
	public Db2Table sb(Integer userId);
	public int insert(Db2Table ta);
	void insertOneDbTranc();
	void insertMulitiTranc();
	void insertMulitiChangeTranc();
}
