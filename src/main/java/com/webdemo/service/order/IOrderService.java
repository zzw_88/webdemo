package com.webdemo.service.order;


import com.webdemo.dal.model.Orders;

public interface IOrderService {
	public Orders selectOrdersFetchPerson(int id);
}
