package com.webdemo.service.order.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webdemo.dal.dao.one.OrdersMapper;
import com.webdemo.dal.model.Orders;
import com.webdemo.service.order.IOrderService;
@Service
public class OrderServiceImpl implements IOrderService {
	@Autowired
	private OrdersMapper orderMapper;
	
	@Override
	public Orders selectOrdersFetchPerson(int id) {
		return orderMapper.selectOrdersFetchPerson(id);
	}
}
