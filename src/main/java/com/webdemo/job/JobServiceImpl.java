package com.webdemo.job;

import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*需要注意的几点：
1、spring的@Scheduled注解  需要写在实现上、
2、 定时器的任务方法不能有返回值（如果有返回值，spring初始化的时候会告诉你有个错误,
需要设定一个proxytargetclass的某个值为true、具体就去百度google吧）
3、实现类上要有组件的注解@Component*/
@Component
//立即加载
@Lazy(false)
public class JobServiceImpl implements IJobService {

	@Scheduled(cron="0/5 * *  * * ?")   //每5秒执行一次  
	@Override
	public void execute() {
		System.out.println("测试定时周期性任务。。。。。。");
	}

}
