﻿var BASE = {
	//ajax提交，不带同步异步标志，返回标志为：resultSuccess，弹出信息为：resultDesc
	ajaxSend : function(url, dataJson,submitType,callback) {
		$.ajax({
			type : (dataJson) ? "POST" : 'GET',
			url : url,
			contentType : "application/x-www-form-urlencoded; charset=utf-8",
			//contentType : "application/json",
			dataType : submitType,
			data : dataJson,
			cache : false,
			success : function(d) {
				 callback(d);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				judgeSession(XMLHttpRequest);
			}
		});
	},
	//ajax提交，带同步异步标志，返回标志为：resultSuccess，弹出信息为：resultDesc
	ajaxSendAsync : function(url, dataJson,submitType,asyncFlag, callback) {
		$.ajax({
			type : (dataJson) ? "POST" : 'GET',
			url : url,
			contentType : "application/x-www-form-urlencoded; charset=utf-8",
			dataType : submitType,
			data : dataJson,
			async:asyncFlag,
			success : function(d) {
				d.resultSuccess == true ? callback(d) : alert(d.resultDesc);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				judgeSession(XMLHttpRequest);
			}
		});
	},
	//ajax提交填充html代码段，不带异步标志，查询条件：dataJson,填充div id名称：divId
	httpSend : function(url, divId, dataJson) {
		divId = divId || 'main';
		$.ajax({
			type : (dataJson) ? "POST" : 'GET',
			url : url,
			contentType : "application/x-www-form-urlencoded; charset=utf-8",
			data : dataJson,
			success : function(d) {
				$('#' + divId).html(d);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				judgeSession(XMLHttpRequest);
			}
		});
	},
	//ajax按钮提交，按钮id名称：bsubmit,不带异步标志，查询条件：dataJson
	ajaxSubmit : function(url, dataJson, callback) {
		var submitBtnName = $("#bsubmit").html();
		$("#bsubmit").attr({
			"disabled" : "disabled"
		}).html('提交中...');
		$.ajax({
			type : (dataJson) ? "POST" : 'GET',
			url : url,
			contentType : "application/x-www-form-urlencoded; charset=utf-8",
			dataType : "json",
			data : dataJson,
			success : function(d) {
				$("#bsubmit").removeAttr("disabled").html(submitBtnName);
				callback(d);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
			    alert("操作失败!");
			    //复原
			    $("#bsubmit").html('保存');
			    $("#bsubmit").removeAttr("disabled");
			}
		});
	}
};

function judgeSession(xtq){
	var sessionstatus=xtq.getResponseHeader("sessionstatus"); 
    if(sessionstatus=="session_null"){ 
        alert("登录超时,请重新登录！");
        window.location.replace("login");   
    }else{
  	    alert("请求出现异常，请稍后重试！");
    }  
}
