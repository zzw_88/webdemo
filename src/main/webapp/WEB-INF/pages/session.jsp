<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%
 	request.getSession().setAttribute("user", null);
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>session超时测试页面</title>
<script src="static/js/base.js"></script>
<script src="static/js/jquery-1.9.1.min.js"></script>
<script>
function sessionTest(){
	$.ajax({
		type : "POST",
		url : 'testSession',
		dataType : "json",
		cache : false,
		success : function(d) {
			 console.log(d);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			judgeSession(XMLHttpRequest);
		}
	});
}
</script>
</head>
<body>
<input type="button" value="session拦截器测试" onClick="sessionTest();" />
</body>
</html>