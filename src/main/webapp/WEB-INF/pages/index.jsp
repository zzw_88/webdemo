<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登录后主页面</title>
</head>
<body>
<h1>功能演示目录:</h1>
<a href="jsonToList">提交json参数,转化为list</a><br />
<a href="jsonToObject">提交json参数,转化为对象(带日期类参数)</a><br />
<a href="testSession">session超时测试页面</a><br />
<a href="testValidate">测试spring mvc validatetion数据校验</a><br />
<a href="testI">sql注入测试</a><br />

<a href="">测试两个库切换：执行单元测试：MulitiDbChangeTest.insertMulitiChangeTranc</a><br />
<a href="">测试跨库事务：执行单元测试：MulitiDbChangeTest.insertMulitiTranc</a><br />
<a href="">测试一对多：执行单元测试：MemberAndOrderTest.testSelectPersonFetchOrder</a><br />
<a href="">测试多对一：执行单元测试：MemberAndOrderTest.testSelectOrdersFetchPerson</a><br />
<a href="">测试spring线程池：执行单元测试：SpringThreadTest.test</a><br />
<a href="">测试事务回滚：执行单元测试：UserServiceTest.testMulitiSqlTranc</a><br />
<a href="">测试自增主键：执行单元测试：UserServiceTest.test1</a><br />
<a href="">测试mybatis类型转换器：执行单元测试：TestHander.insert</a><br />
<a href="">测试ehcache缓存：执行单元测试：CacheTest.testSelectCache</a><br />
</body>
</html>