<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>提交json转化为list</title>
<script src="static/js/base.js"></script>
<script src="static/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
function ajax1(){
	//封装json字符串
	var param = JSON.stringify(
			[{"userId":"1","username":"a","password":"a"},{"userId":"1","username":"b","password":"b"}]
	);
	$.ajax({
		type : "POST",
		url : 'jsonToList',
		//contentType : " text/plain",
		//contentType : " text/html",
		contentType : "application/json",
		dataType : "json",
		data :param,
		cache : false,
		success : function(d) {
			//alert(d);
			alert(d.success);
			//console.log(d);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			judgeSession(XMLHttpRequest);
		}
	});
}
</script>
</head>
<body>
<h1>异步提交json参数，spring后台接收后处理为list</h1>
<input type="button" onClick="ajax1();" value="异步提交json参数" />
 <form action="jsonToList" method="post">
	<input type="text"  value="11"  />
	<input type="submit" value="提交" />
</form>
</body>
</html>