/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50096
Source Host           : localhost:3306
Source Database       : webdemo

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2016-02-05 09:02:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `p_id` int(11) NOT NULL auto_increment,
  `name` varchar(32) default NULL,
  PRIMARY KEY  (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', '张三');
INSERT INTO `member` VALUES ('2', '李四');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `o_id` int(11) NOT NULL auto_increment,
  `price` decimal(11,0) default NULL,
  `p_id` int(11) default NULL,
  PRIMARY KEY  (`o_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '5', '1');
INSERT INTO `orders` VALUES ('2', '6', '1');
INSERT INTO `orders` VALUES ('3', '7', '2');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userId` int(8) NOT NULL auto_increment,
  `username` varchar(32) default NULL,
  `password` varchar(32) default NULL,
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'test1_u', 'test1_p');
INSERT INTO `users` VALUES ('2', 'test1_u', 'test1_p');
INSERT INTO `users` VALUES ('9', 'test1_s', 'test1_s');
INSERT INTO `users` VALUES ('10', 'test1_u', 'test1_p');
INSERT INTO `users` VALUES ('11', 'test1_s', 'test1_s');
INSERT INTO `users` VALUES ('12', 'test1_u', 'test1_p');
INSERT INTO `users` VALUES ('13', 'test1_s', 'test1_s');
INSERT INTO `users` VALUES ('14', 'test1_u', 'test1_p');
INSERT INTO `users` VALUES ('15', 'test1_s', 'test1_s');
INSERT INTO `users` VALUES ('16', 'test1_s', 'test1_s');
INSERT INTO `users` VALUES ('17', 'test1_u', 'test1_p');
