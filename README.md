#webdemo  
1.基于spring mvc3 mybatis ehcache  
2.架构及功能：  
(1)分层：  
dal (dao model) service  controller view 四层  
dao层：mybatis获取插入的自增主键，一对多，多对一示例  
view视图层：输出pdf类-PdfRevenueReportView 输出Excel类-ViewExcel  
(2)辅助工具类：  
快速访问spring管理bean-SpringUtil  
日期编辑器类-DateEditor  
(3)封装的异常类：  
项目异常类 -WebdemoException   
session超时异常类-SessionTimeoutException  
(4)封装的枚举类：  
系统操作码及对应描述-SystemCodeEnum  
(5)请求返回处理：DataUtils  
(6)封装的拦截器请求类：  
session超时及登录验证-SessionTimeoutInterceptor  
(7)处理定时任务：JobServiceImpl  
(7)单元测试：  
测试基类-BaseServiceTest   
缓存测试类：CacheTest TestCacheImpl   
事务测试类：UserServiceTest   

